package Assignment03.bankApplication;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

class TransactionHistory {

	String transactionType;
	double amount;

	TransactionHistory(String transactionType, double amount) {

		this.transactionType = transactionType;
		this.amount = amount;
	}

	@Override
	public String toString() {
		return transactionType + ":" + "\t" + "$" + amount;
	}

}

class Account {

	static double balance = 0;

	static ArrayList<TransactionHistory> transactionHistory = new ArrayList<TransactionHistory>();
	// logic username pw name
	static HashMap<String, String> userInnerCredentials = new HashMap<String, String>();

	static HashMap<String, HashMap<String, String>> userCredentials = new HashMap<String, HashMap<String, String>>();

	static void setUpAccountDB() {
		// Store credentials
		userInnerCredentials.put("pw@1234", "Edmund");
		userCredentials.put("user1234", userInnerCredentials);
	}

	static void deposit(double amount) throws Exception {

		if (amount > 0) {
			balance += amount;
			transactionHistory.add(new TransactionHistory("Deposited", amount));
			System.out.println("Deposited $" + amount + "... Back to menu");
		} else {
			System.out.println("Invalid amount... Back to menu");
		}
		displayMenu();
	}

	static void withdraw(double amount) throws Exception {

		// Checking balance
		if (balance > amount) {
			balance -= amount;
			transactionHistory.add(new TransactionHistory("Withdrawn", amount));
			System.out.println("Withdrawn $" + amount + "... Back to menu");
		} else {
			System.out.println("Insufficent Fund... Back to menu");

		}
		displayMenu();
	}

	static void getTransactionHistory() throws Exception {

		if (transactionHistory.isEmpty()) {
			System.out.println("No transaction made.. Back to menu");
		} else {
			System.out.println("Transaction History");
			System.out.println("-------------------");
			System.out.println("Type \t\t Amount");
			for (TransactionHistory item : transactionHistory) {
				System.out.println(item);
			}
		}
		displayMenu();

	}

	static void calculateInterest(int years) throws Exception {

		// 2 decimal point
		final DecimalFormat df = new DecimalFormat("0.00");
		if (years > 0) {
			double interestRate = 0.0110;
			double newBalance = balance + (balance * interestRate * years);
			System.out.println("The current interest rate is " + df.format(100 * interestRate) + "%");
			System.out.println(
					"After " + years + " years, your balance will be: $" + df.format(newBalance) + "... Back to menu");
		} else {
			System.out.println("Invalid input years... Back to menu");
		}

		displayMenu();

	}

	static void displayMenu() throws Exception {
		int choiceInput;
		double amount;
		int years;
		System.out.println();
		System.out.println("What would you like to do?");
		System.out.println();
		System.out.println("1. Check your balance");
		System.out.println("2. Make a deposit");
		System.out.println("3. Make a withdrawal");
		System.out.println("4. View transaction history");
		System.out.println("5. Calculate interest");
		System.out.println("6. Exit");
		try {
			do {
				System.out.println();
				System.out.println("Enter your choice: ");
				Scanner sc = new Scanner(System.in);
				choiceInput = sc.nextInt();

				switch (choiceInput) {

				case 1:
					System.out.println("Balance= $" + balance);
					break;

				case 2:
					System.out.println("Enter amount to deposit: ");
					amount = sc.nextDouble();
					deposit(amount);
					break;
				case 3:
					System.out.println("Enter amount to withdraw: ");
					amount = sc.nextDouble();
					withdraw(amount);
					break;
				case 4:

					getTransactionHistory();
					break;
				case 5:
					System.out.println("Enter number of years for interest: ");
					years = sc.nextInt();
					calculateInterest(years);
					break;
				case 6:
					System.out.println("Thank you for banking with us!");
					System.exit(0);
					break;
				default:
					System.out.println("Invalid option, try again!");
					continue;
				}
			} while (!(choiceInput < 1 && choiceInput > 6));
		} catch (Exception e) {
			System.out.println("Invalid input... displaying menu again...");
		} finally {
			displayMenu();
		}
	}

	static void enterCredPrompt() {
		String name;
		String usernameInput;
		String passwordInput;
		do {
			System.out.println("Please enter your username:");
			Scanner sc = new Scanner(System.in);
			usernameInput = sc.next();
			System.out.println("Please enter your password:");
			passwordInput = sc.next();
			if (userCredentials.containsKey(usernameInput) == false
					|| (userCredentials.get(usernameInput).get(passwordInput) == null)) {
				System.out.println("Invalid username/password, please try again!");
				System.out.println("--------------------------------------------");
				System.out.println();
			}
		} while (userCredentials.containsKey(usernameInput) == false
				|| (userCredentials.get(usernameInput).get(passwordInput) == null));

		name = userCredentials.get(usernameInput).get(passwordInput);
		System.out.println();
		System.out.println("Welcome " + name + "!");

	}

}

public class LaunchBankApplication extends Account {

	public static void main(String[] args) throws Exception {

		try {
			setUpAccountDB();

			System.out.println("-------------------");
			System.out.println("Welcome to the Bank");
			System.out.println("-------------------");
			enterCredPrompt();
			displayMenu();
		} catch (Exception e) {
			System.out.println("Something is wrong... System rebooting...");
		} finally {
			setUpAccountDB();

			System.out.println("-------------------");
			System.out.println("Welcome to the Bank");
			System.out.println("-------------------");
			enterCredPrompt();
		}

	}

}
